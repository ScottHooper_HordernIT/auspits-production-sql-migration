BEGIN TRANSACTION

	--SQL PAYLOAD
	ALTER TABLE dbo.Pits ADD
	DatePitRequired datetime NULL
	GO
	IF @@ERROR <> 0 SET NOEXEC ON
	GO
	
	CREATE TABLE [dbo].[DeliveryDrivers](
		[DeliveryDriverID] [int] IDENTITY(1,1) NOT NULL,
		[FirstName] [varchar](50) NULL,
		[LastName] [varchar](50) NULL,
		[CompanyName] [varchar](50) NULL,
		[EmailAddress] [varchar](50) NOT NULL,
	 CONSTRAINT [PK__Delivery__B78ABB161F839E7F] PRIMARY KEY CLUSTERED 
	(
		[DeliveryDriverID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	GO
	IF @@ERROR <> 0 SET NOEXEC ON
	GO
	
	CREATE TABLE dbo.CheckList (
		[ChecklistID] [int] IDENTITY(1,1) NOT NULL,
		[PitId] [int] NOT NULL,
		[PitType] [nvarchar](255) NOT NULL,
		[ProjectId] [int] NOT NULL,
		[PitNo] [nvarchar](255) NOT NULL,
		[QuoteNo] [nvarchar](255) NULL,
		[RevisionCode] [nvarchar](255),
		[Comments_B] [int] NULL,
		[Comments_C] [int] NULL,
		[BoxingDimensions_B] [int] NULL,
		[BoxingDimensions_C] [int] NULL,
		[BoxingHeights_B] [int] NULL,
		[BoxingHeights_C] [int] NULL,
		[BoxingRebate_B] [int] NULL,
		[BoxingRebate_C] [int] NULL,
		[Reinforcement_B] [int] NULL,
		[Reinforcement_C] [int] NULL,
		[ReinforcementCover_B] [int] NULL,
		[ReinforcementCover_C] [int] NULL,
		[ReinforcementHoldPoint_B] [int] NULL,
		[ReinforcementHoldPoint_C] [int] NULL,
		[MouldCleanJoints_B] [int] NULL,
		[MouldCleanJoints_C] [int] NULL,
		[MouldConduitSize_B] [int] NULL,
		[MouldConduitSize_C] [int] NULL,
		[MouldConduitPosition_B] [int] NULL,
		[MouldConduitPosition_C] [int] NULL,
		[MouldFerrules_B] [int] NULL,
		[MouldFerrules_C] [int] NULL,
		[MouldLiftAnchors_B] [int] NULL,
		[MouldLiftAnchors_C] [int] NULL,
		[ExternalBoxingBlockout_B] [int] NULL,
		[ExternalBoxingBlockout_C] [int] NULL,
		[ExternalBoxingHoldPoint_B] [int] NULL,
		[ExternalBoxingHoldPoint_C] [int] NULL,
		[PouringMix] [int] NULL,
		[PouringSpread] [int] NULL,
		[PouringEvaporation] [int] NULL,
		[PouringWashMould] [int] NULL,
		[PopStripCracks] [int] NULL,
		[PopStripFormwork] [int] NULL,
		[PopStripBugHoles] [int] NULL,
		[PopStripSteps] [int] NULL,
		[CuringCompound] [int] NULL,
		[CuringCompoundDate] [datetime] NULL,
		[CuringStart] [int] NULL,
		[CuringStartDate] [datetime] NULL,
		[CuringEnd] [int] NULL,
		[CuringEndDate] [datetime] NULL,
		[FinalLabel] [int] NULL,
		[FinalComments] [int] NULL,
		[FinalDimension] [int] NULL,
		[FinalBlockoutsConduitsSize] [int] NULL,
		[FinalBlockoutsConduitsPosition] [int] NULL,
		[FinalVisual] [int] NULL
	)
	GO
	IF @@ERROR <> 0 SET NOEXEC ON
	GO

COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END