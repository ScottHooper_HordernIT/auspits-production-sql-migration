Add 'Required Date' to Ordered Pits
	schema update
	repoint db back to prod (Module - RepointDb)
	delete tmpImportPits (for automatic recreation with new field)
	form design & module changes - ImportPits

Email Delivery Dockets to Drivers
	schema update (create table DeliveryDrivers)
	add table to access db
	create passthrough query _DeliveryDrivers
	Create form DeliveryDocketEmailSelect
	Edit DD email button on LoadingDocket form
	Added VBA reference to Microsoft VBScript Regular Expressions 5.5

Electronic Shop Card Checklist
	schema update (create table Checklist)
	add table to access
	create passthrough query _Checklist
